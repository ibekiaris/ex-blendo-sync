<?php
return [
    // Services - Dependencies
    'dependencies' => [
        'factories' => [
            \App\Command\SyncListCommand::class => \App\Command\Factory\SyncListCommandFactory::class,
            \App\Command\SyncItemCommand::class => \App\Command\Factory\SyncItemCommandFactory::class,
            // Models
            \App\Model\Resource\ObjectResource::class => \App\Model\Resource\Factory\ObjectResourceFactory::class,
            \App\Model\Resource\TrackingResource::class => \App\Model\Resource\Factory\TrackingResourceFactory::class,
            \App\Model\Resource\SubscriptionResource::class => \App\Model\Resource\Factory\SubscriptionResourceFactory::class,
            \App\Model\Resource\SubscriptionResourceLocal::class => \App\Model\Resource\Factory\SubscriptionResourceLocalFactory::class,
            // Services
            \App\Service\Factory\StorageServiceFactory::class => \App\Service\Factory\StorageServiceFactory::class,
            \App\Service\SyncService::class => \App\Service\Factory\SyncServiceFactory::class,
        ],
    ],
    'commands' => [
        \App\Command\SyncListCommand::class,
        \App\Command\SyncItemCommand::class,
    ],
    'objects' => [
        [
            'id' => 1,
            'alias' => 'subscription',
            'remote_resource' => \App\Model\Resource\SubscriptionResource::class,
            'local_resource' => \App\Model\Resource\SubscriptionResourceLocal::class,
        ]
    ],
    'db' => [
        'mongo' => [
            'db_name' => 'sync',
        ]
    ]
];