<?php
namespace App\Command\Factory;

use App\Command\SyncListCommand;
use App\Model\SyncModel;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory Class for SyncListCommand
 *
 * @category   CategoryName
 * @package    App\Command\Factory
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class SyncListCommandFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $syncModel = new SyncModel($container);
        return new SyncListCommand($syncModel);
    }
}
