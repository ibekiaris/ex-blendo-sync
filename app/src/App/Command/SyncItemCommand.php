<?php
namespace App\Command;

use App\Model\SyncModel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *  This Command updates or inserts an Item
 *
 * @category   CategoryName
 * @package    App\Command
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class SyncItemCommand extends Command
{
    const SYNC_ITEM_COMMAND_NAME = 'sync.item';

    /**
     * @var SyncModel
     */
    protected $syncModel;

    public function __construct(SyncModel $syncModel)
    {
        $this->syncModel = $syncModel;
        parent::__construct(self::SYNC_ITEM_COMMAND_NAME);
    }

    protected function configure()
    {
        $this->setDefinition(
            new InputDefinition([
                new InputOption('id', 'i', InputOption::VALUE_REQUIRED),
            ])
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getOption('id');
        $this->syncModel->syncItem($id);
    }
}
