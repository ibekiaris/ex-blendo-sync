<?php
namespace App\Command;

use App\Model\SyncModel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This Command checks a resource for changes
 *
 * @category   CategoryName
 * @package    App\Command
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class SyncListCommand extends Command
{
    const SYNC_LIST_COMMAND_NAME = 'sync.list';

    /**
     * @var SyncModel
     */
    protected $syncModel;

    public function __construct(SyncModel $syncModel)
    {
        $this->syncModel = $syncModel;
        parent::__construct(self::SYNC_LIST_COMMAND_NAME);
    }

    protected function configure()
    {
        $this->setDefinition(
            new InputDefinition([
                new InputOption('objectAlias', 'o', InputOption::VALUE_REQUIRED),
                new InputOption('offset', 'f', InputOption::VALUE_OPTIONAL, 10),
                new InputOption('limit', 'l', InputOption::VALUE_OPTIONAL, 0),
            ])
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $objectAlias = $input->getOption('objectAlias');
        $offset = $input->getOption('offset');
        $limit = $input->getOption('limit');
        $this->syncModel->syncList($objectAlias, $limit, $offset);
    }
}
