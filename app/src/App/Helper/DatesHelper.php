<?php
namespace App\Helper;

use MongoDB\BSON\UTCDateTime;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Helper
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class DatesHelper
{
    /**
     * Turns timestamp in seconds into UTCDateTime
     *
     * @param int $timestamp
     *
     * @return UTCDateTime
     */
    public static function getDateObjectFromTimestamp($timestamp)
    {
        return new UTCDateTime($timestamp * 1000);
    }
}
