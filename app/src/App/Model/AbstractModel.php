<?php
namespace App\Model;

use Interop\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class AbstractModel
{
    /**
     * @var ContainerInterface
     */
    protected $serviceManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(ContainerInterface $container)
    {
        $this->serviceManager = $container;
    }
}
