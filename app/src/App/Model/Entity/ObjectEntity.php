<?php
namespace App\Model\Entity;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Entity
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class ObjectEntity
{
    /**
     * @var string Item Id
     */
    protected $id;

    /**
     * @var string Object Alias
     *
     * @see config.php
     */
    protected $alias;

    /**
     * @var string Remote Resource Class
     */
    protected $remoteResource;

    /**
     * @var string Locale Resource Class
     */
    protected $localResource;

    public function __construct($data)
    {
        $this->itemId = isset($data['item_id']) ? $data['id'] : null;
        $this->objectAlias = isset($data['object_alias']) ? $data['alias'] : null;
        $this->remoteResource = isset($data['remote_resource']) ? $data['remote_resource'] : null;
        $this->localResource = isset($data['local_resource']) ? $data['local_resource'] : null;
    }

    /**
     * @return mixed
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @return mixed
     */
    public function getObjectAlias()
    {
        return $this->objectAlias;
    }

    /**
     * @return mixed
     */
    public function getRemoteResource()
    {
        return $this->remoteResource;
    }

    /**
     * @return mixed
     */
    public function getLocalResource()
    {
        return $this->localResource;
    }
}
