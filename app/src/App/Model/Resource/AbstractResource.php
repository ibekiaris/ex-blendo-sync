<?php
namespace App\Model\Resource;

use Interop\Container\ContainerInterface;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Resource
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class AbstractResource
{
    /**
     * @var ContainerInterface
     */
    protected $serviceManager;

    public function __construct(ContainerInterface $container)
    {
        $this->serviceManager = $container;
    }
}
