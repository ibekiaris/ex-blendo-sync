<?php
namespace App\Model\Resource\Contract;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Resource\Contract
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
interface LocalResourceInterface
{
    public function getById($id);

    public function delete($id);

    public function update($id, $data);

    public function create($data);
}
