<?php
namespace App\Model\Resource\Contract;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Resource\Contract
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
interface ObjectResourceInterface
{
    public function getObjectByAlias($alias);
}
