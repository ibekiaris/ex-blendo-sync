<?php
namespace App\Model\Resource\Contract;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Resource\Contract
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
interface TrackingResourceInterface
{
    public function getByItemId($id);

    public function getAndLockItem($id);

    public function create($data);

    public function update($id, $data);

    public function deleteByItemId($id);
}
