<?php
namespace App\Model\Resource\Factory;

use App\Model\Resource\ObjectResource;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory Class for ObjectResource
 *
 * @category   CategoryName
 * @package    App\Model\Resource\Factory
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class ObjectResourceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ObjectResource($container);
    }
}
