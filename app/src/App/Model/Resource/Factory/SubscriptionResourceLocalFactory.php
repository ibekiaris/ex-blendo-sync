<?php
namespace App\Model\Resource\Factory;

use App\Model\Resource\SubscriptionResourceLocal;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Resource\Factory
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class SubscriptionResourceLocalFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new SubscriptionResourceLocal($container);
    }
}
