<?php
namespace App\Model\Resource;

use App\Model\Entity\ObjectEntity;
use App\Model\Resource\Contract\ObjectResourceInterface;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Resource\Contract
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class ObjectResource extends AbstractResource implements ObjectResourceInterface
{
    public function getObjectByAlias($alias)
    {
        $objectsConfig = $this->serviceManager->get('config')['objects'];

        foreach ($objectsConfig as $value) {
            if (isset($value['alias']) && $alias == $value['alias']) {
                return new ObjectEntity($value);
                break;
            }
        }

        return null;
    }
}
