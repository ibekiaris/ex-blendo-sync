<?php
namespace App\Model\Resource;
use App\Model\Resource\Contract\RestResourceInterface;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Resource
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class SubscriptionResource extends AbstractResource implements RestResourceInterface
{
    public function getList($limit, $offset, $filters = array())
    {
        $subscriptions = [];

        $all = \ChargeBee_Subscription::all([
            'limit' => $limit,
            'sortBy[asc]' => 'created_at'
        ]);

        foreach($all as $entry) {
            $subscriptions[] = $entry->subscription();
        }

        return $subscriptions;
    }

    public function getItem($id)
    {
        $result = \ChargeBee_Subscription::retrieve($id);
        return $result->subscription();
    }
}
