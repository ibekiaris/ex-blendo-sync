<?php
namespace App\Model\Resource;

use App\Model\Resource\Contract\LocalResourceInterface;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Resource
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class SubscriptionResourceLocal extends AbstractResource implements LocalResourceInterface
{
    protected $collection = 'subscriptions';

    public function getById($id)
    {
        $db = $this->serviceManager->get('App\Service\Factory\StorageServiceFactory');
        return $db->selectCollection($this->collection)->findOne(['id' => $id]);
    }

    public function delete($id)
    {
        $db = $this->serviceManager->get('App\Service\Factory\StorageServiceFactory');
        return $db->selectCollection($this->collection)->remove(['id' => $id]);
    }

    public function update($id, $data)
    {
        $db = $this->serviceManager->get('App\Service\Factory\StorageServiceFactory');
        return $db->selectCollection($this->collection)->updateOne(['id' => $id], ['$set' => $data]);
    }

    public function create($data)
    {
        $db = $this->serviceManager->get('App\Service\Factory\StorageServiceFactory');
        return $db->selectCollection($this->collection)->insertOne($data);
    }
}
