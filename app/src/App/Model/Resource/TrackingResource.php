<?php
namespace App\Model\Resource;

use App\Model\Resource\Contract\TrackingResourceInterface;
use App\Service\SyncService;
use MongoDB\BSON\ObjectID;
use MongoDB\Client;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model\Resource
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class TrackingResource extends AbstractResource implements TrackingResourceInterface
{
    protected $collection = 'tracking';

    public function getByItemId($id)
    {
        $db = $this->serviceManager->get('App\Service\Factory\StorageServiceFactory');
        return $db->selectCollection($this->collection)->findOne(
            ['item_id' => $id]
        );
    }

    public function getAndLockItem($id)
    {
        $db = $this->serviceManager->get('App\Service\Factory\StorageServiceFactory');
        $query = ['status' => SyncService::TRACKING_STATUS_OUTDATED];

        if ($id) {
            $query['id'] = $id;
        }

        return $db->selectCollection($this->collection)->findOneAndUpdate(
            $query,
            ['$set' => ['status' => SyncService::TRACKING_STATUS_UPDATED]],
            ['sort' => ['updated_date' => -1]]
        );
    }

    public function create($data)
    {
        $db = $this->serviceManager->get('App\Service\Factory\StorageServiceFactory');
        return $db->selectCollection($this->collection)->insertOne($data);
    }

    public function update($id, $data)
    {
        $db = $this->serviceManager->get('App\Service\Factory\StorageServiceFactory');
        return $db->selectCollection($this->collection)->updateOne(['_id' => new ObjectID($id)], ['$set' => $data]);
    }

    public function deleteByItemId($id)
    {

    }
}
