<?php
namespace App\Model;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Model
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class SyncModel extends AbstractModel
{
    public function syncList($objectAlias, $limit = 10, $offset = 0)
    {
        // Step A: Fetch Remote Data
        $object = $this->serviceManager->get('App\Model\Resource\ObjectResource')->getObjectByAlias($objectAlias);
        if (! $remoteData = $this->serviceManager->get($object->getRemoteResource())->getList($limit, $offset)) {
            return;
        };

        // Step B: Sync Tracking Collection
        $this->serviceManager->get('App\Service\SyncService')->syncTrackingCollection($remoteData, $objectAlias);
    }

    public function syncItem($id = null)
    {
        // Step A: Fetch and Lock Tracking entry
        $syncService = $this->serviceManager->get('App\Service\SyncService');

        if (! $trackingEntry = $syncService->fetchTrackingEntryToUpdate($id)) {
            return;
        }

        // Step B: Fetch Remote data
        $object = $this->serviceManager->get('App\Model\Resource\ObjectResource')->getObjectByAlias($trackingEntry->object);
        $item = $this->serviceManager->get($object->getRemoteResource())->getItem($trackingEntry->item_id);

        // Step C: Sync Item Data
        $syncService->setLocalResource($this->serviceManager->get($object->getLocalResource()))->syncLocalWithRemoteItem($trackingEntry->item_id, $item);

        // Step D: If previous steps fails update tracking entry status to 1 - Catch exception
    }
}
