<?php
namespace App\Service\Contract;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Service\Contract
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
interface StorageServiceInterface
{
    public function getConnection();
}
