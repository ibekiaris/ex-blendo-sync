<?php
namespace App\Service\Factory;

use Interop\Container\ContainerInterface;
use MongoDB\Client;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Service\Factory
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class StorageServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $connection = new Client();
        $config = $container->get('config')['db']['mongo'];
        return $connection->selectDatabase($config['db_name']);
    }
}
