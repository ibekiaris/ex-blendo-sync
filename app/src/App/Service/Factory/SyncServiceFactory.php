<?php
namespace App\Service\Factory;

use App\Service\SyncService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory Class for SyncService
 *
 * @category   CategoryName
 * @package    App\Service\Factory
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class SyncServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new SyncService($container);
    }
}
