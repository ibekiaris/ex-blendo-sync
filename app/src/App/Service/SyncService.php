<?php
namespace App\Service;

use App\Helper\DatesHelper;
use App\Model\Resource\Contract\LocalResourceInterface;
use Interop\Container\ContainerInterface;

/**
 * Short description for file
 *
 * @category   CategoryName
 * @package    App\Service
 * @author     Ioannis Bekiaris <info@ibekiaris.me>
 * @copyright  2016 - 2017 Ioannis Bekiaris
 */
class SyncService
{
    const TRACKING_STATUS_OUTDATED = 1;
    const TRACKING_STATUS_UPDATED = 0;

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @var LocalResourceInterface
     */
    protected $localResource;

    /**
     * This method is used to sync
     * local Items (local storage) with remote Items (API)
     *
     * Sync method does the following:
     *
     * - Deletes local item + and respective tracking entry if remote data is null
     * - Creates a new entry if remote item not exist in local storage
     * - Updates respective local item that has different date than the remote item
     *
     * @param string $id Item ID
     * @param mixed $remoteEntity
     */
    public function syncLocalWithRemoteItem($id, $remoteEntity = null)
    {
        // TODO Format Remote Data + Use Hydrators. Maybe move that or resources that return data is better
        // TODO Think about moving some logic to db (but is more clear to do the way it is now in order to change storage)

        if (!$remoteEntity) {
            $this->localResource->delete($id);
            $this->container->get('App\Model\Resource\TrackingResource')->deleteByItemId($id);
            return;
        }

        if (!$item = $this->localResource->getById($id)) {
            $this->localResource->create($remoteEntity);
            return;
        }

        /**
         * Maybe the best way is move that comparison in a method
         * e.g $item->isOutDated()
         * However for that exercise we are going to use plain object without behavior in order to gain some time
         */
        if ($item->updated_at < $remoteEntity->updated_at) {
            $this->localResource->update($id, $remoteEntity);
        }

        return;
    }

    /**
     * Sync Tracking collection against remote data
     *
     * - Set status 1 and set new update date to local Entries
     *   if they have older updated date than the one remote.
     *
     * - Create new Entries with status 1 for items not exist
     *
     * @param $remoteData
     * @param $objectAlias
     */
    public function syncTrackingCollection($remoteData, $objectAlias)
    {
        $trackingResource = $this->container->get('App\Model\Resource\TrackingResource');

        foreach ($remoteData as $remoteEntity) {

            $itemId = $remoteEntity->id;

            if (! $trackingEntity = $trackingResource->getByItemId($itemId)) {

                $entry = [
                    'item_id' => $itemId,
                    'object' => $objectAlias,
                    'updated_date' => DatesHelper::getDateObjectFromTimestamp($remoteEntity->updatedAt),
                    'status' => self::TRACKING_STATUS_OUTDATED,
                ];

                $trackingResource->create($entry);
                continue;
            }

            /**
             * Maybe the best way is move that comparison in a method of tracking entity object
             * e.g $trackingEntity->isOutDated()
             * However for that exercise we are going to use plain object without behavior in order to gain some time
             */
            if ($trackingEntity->updated_date->toDateTime()->format('U') <  $remoteEntity->updatedAt) {
                $trackingEntity->updated_date = DatesHelper::getDateObjectFromTimestamp($remoteEntity->updated_date);
                $trackingResource->update($itemId, $trackingEntity);
            }
        }
    }

    /**
     * Lock and fetch Tracking Entry. Lock means
     * set Tracking Entry status to 0
     *
     * If $id or $objectAlias is null the method returns
     * the entry with the newest update date with status 1
     *
     * @param $id
     *
     * @return mixed
     */
    public function fetchTrackingEntryToUpdate($id = null)
    {
        return $this->container->get('App\Model\Resource\TrackingResource')->getAndLockItem($id);
    }

    /**
     * @param  LocalResourceInterface $localResource
     * @return SyncService
     */
    public function setLocalResource($localResource)
    {
        $this->localResource = $localResource;
        return $this;
    }
}
