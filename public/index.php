<?php
use Symfony\Component\Console\Application;

if (php_sapi_name() != "cli") {
    exit(1);
}

chdir(dirname(__DIR__));

require 'vendor/autoload.php';
$container = require 'app/config/container.php';
$commands = $container->get('config')['commands'];

$application = new Application('sync');

foreach ($commands as $command) {
    $application->add($container->get($command));
}

ChargeBee_Environment::configure(
    $container->get('config')['api']['site'],
    $container->get('config')['api']['key']
);

$application->run();
